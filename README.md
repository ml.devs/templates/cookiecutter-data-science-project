# Cookiecutter Data Science Project
Data Science project structure template. It is intended that this template will be used to create a project for building pipelines, and the research, on the basis of which steps will be added to the pipeline, will be done in separate submodules connected with this project.

## Requirements to use the cookiecutter template
- `Python`>=3.10
- [`Cookiecutter Python package`][cookiecutter_docs]>= 2.1
- `Git` >= 2.40

## Getting Started
It is recommended to use `micromamba` to manage dependencies and create virtual environments. You can find installation instructions in `{{ cookiecutter.repo_name }}/README.md`.

Create a virtual environment with `python`, `cookiecutter` and `git`:
```bash
micromamba create -n cookiecutter "python>=3.10" "cookiecutter>=2.1" "git>=2.40" -c conda-forge
```

Activate the virtual environment:
```bash
micromamba activate cookiecutter
```

To start a new project, run:
```bash
cookiecutter https://gitlab.com/ml.devs/templates/cookiecutter-data-scientist
```

## Project Organization
```
├── README.md
│
├── {{ cookiecutter.repo_name }}/
│   │
│   ├── src/                      <- Source code for use in `Snakemake` rules.
│   │   │
│   │   ├── conf/                 <- `Hydra` configuration.
│   │   │   │
│   │   │   ├── data/             <- `Hydra` group for data sources configs.
│   │   │   │   │
│   │   │   │   └── lakefs.yaml   <- `LakeFS` configuration.
│   │   │   │
│   │   │   └── config.yaml       <- Main config for `Hydra` default values.
│   │   │
│   │   ├── config.py             <- Example of `Hydra` config compose usage.
│   │   │
│   │   └── __init__.py
│   │
│   ├── __init__.py
│   │
│   ├── {pipeline name 1}.smk     <- `Snakemake` pipeline.
│   │
│   ├── ...
│   │
│   └── {pipeline name n}.smk
│
├── research/                     <- Git submodules for researchers `Quarto` notebooks.
│
├── _quarto.yaml                  <- `Quarto` project config.
│
├── index.qmd                     <- `Quarto` website main page.
│
├── styles.css                    <- `Quarto` custom css styles.
│
├── devenv.yaml                   <- `Micromamba` environment file for
│                                    binary dependencies and python version.
└── pyproject.toml                <- Python dependencies for `Poetry`.
```

## Template options
- **author_name**: "John Doe"
- **author_email**: "doe@john.com"
- **project_name**: "Project name"
- **description**: "A short description of the project."
- **repo_name**: "{project-name}-{author-name}"
- **dev_env_name**: "{project-name}-denv"
- **python_version**: "3.10"
- **poetry_version**: "1.4"
- **quarto_version**: "1.3"
- **git_version**: "2.40"
- **snakemake_version**: "7.25"
- **version**: "0.1.0"

[cookiecutter_docs]: http://cookiecutter.readthedocs.org/en/latest/installation.html
