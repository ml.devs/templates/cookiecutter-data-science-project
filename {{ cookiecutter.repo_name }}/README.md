# {{ cookiecutter.project_name }}
{{ cookiecutter.description }}

## Getting Started
This is a data science project template for working with data hosted in a remote storage, where research are done in separate submodules connected to this project and formatted as `quarto` notebooks, utility source code is formatted as a python library, dependencies are managed using ` poetry` and `micromamba`, configurations are managed using `hydra` and pipelines are built using `snakemake`.

## Project Organization
```
├── README.md
│
├── {{ cookiecutter.repo_name }}/
│   │
│   ├── src/                      <- Source code for use in `Snakemake` rules.
│   │   │
│   │   ├── conf/                 <- `Hydra` configuration.
│   │   │   │
│   │   │   ├── data/             <- `Hydra` group for data sources configs.
│   │   │   │   │
│   │   │   │   └── lakefs.yaml   <- `LakeFS` configuration.
│   │   │   │
│   │   │   └── config.yaml       <- Main config for `Hydra` default values.
│   │   │
│   │   ├── config.py             <- Example of `Hydra` config compose usage.
│   │   │
│   │   └── __init__.py
│   │
│   ├── __init__.py
│   │
│   ├── {pipeline name 1}.smk     <- `Snakemake` pipeline.
│   │
│   ├── ...
│   │
│   └── {pipeline name n}.smk
│
├── research/                     <- Git submodules for researchers `Quarto` notebooks.
│
├── _quarto.yaml                  <- `Quarto` project config.
│
├── index.qmd                     <- `Quarto` website main page.
│
├── styles.css                    <- `Quarto` custom css styles.
│
├── devenv.yaml                   <- `Micromamba` environment file for
│                                    binary dependencies and python version.
└── pyproject.toml                <- Python dependencies for `Poetry`.
```

## System requirements
- `git` 2.40+
- `python` 3.10+
- `micromamba` 1.3+
- `poetry` 1.4+
- `quarto` 1.2+
- `snakemake` 7.25+

### Micromamba installation
If you are using Mac OS or Linux, there is a simple way of installing `micromamba`. Simply execute the installation script in your preferred shell.

For Linux, the default shell is bash:
```bash
curl micro.mamba.pm/install.sh | bash
```

For Mac OS, the default shell is zsh:
```zsh
curl micro.mamba.pm/install.sh | zsh
```

If you are using Windows, below are the commands to get `micromamba` installed in PowerShell.
```powershell
Invoke-Webrequest -URI https://micro.mamba.pm/api/micromamba/win-64/latest -OutFile micromamba.tar.bz2
tar xf micromamba.tar.bz2

MOVE -Force Library\bin\micromamba.exe micromamba.exe
.\micromamba.exe --help

# You can use e.g. $HOME\micromambaenv as your base prefix
$Env:MAMBA_ROOT_PREFIX="C:\Your\Root\Prefix"

# Invoke the hook
.\micromamba.exe shell hook -s powershell | Out-String | Invoke-Expression

# ... or initialize the shell
.\micromamba.exe shell init -s powershell -p C:\Your\Root\Prefix
```

## Initial setup

### Create virtual environment
Create and activate `micromamba` virtual environment:

```bash
micromamba env create -f devenv.yaml
micromamba activate {{ cookiecutter.dev_env_name }}
```

You will get a virtual environment with `python`, `poetry`, `quarto`, `git` and `snakemake` installed with the required versions.

### Git initial setup
Create git repository in {{ cookiecutter.repo_name }}, use `main` as `initial-branch` name:
```bash
git init --initial-branch=main
```

Set valid username and email:
```bash
git config user.name "{{ cookiecutter.author_name }}"
git config user.email "{{ cookiecutter.author_email }}"
```

### Poetry initial setup
Configure `poetry`:
```bash
poetry config virtualenvs.in-project false --local
poetry config virtualenvs.path ~/micromamba/envs --local
```

### Install dependencies
Install dependencies with `poetry`:
```bash
poetry install
```

### Nitpick configuration files setup
`Nitpick` use to enforce the same configuration files settings across projects.

To get of fix your configuration files (`pyproject.toml`, `.pre-commit-config.yaml`, etc.) run:
```
nitpick fix
```

To check the configuration files or examine what will be added, use:
```
nitpick check
```

### Pre-commit initial setup
**Make sure you have a `.pre-commit-config.yaml` file, otherwise go back to the previous step with the `nitpick.`**

Configure `pre-commit` hooks:

```bash
pre-commit install --hook-type pre-commit --hook-type pre-push --hook-type commit-msg
```

### Work with sensitive environment variables
Environment variables are used to work with sensitive information. For convenience, you can take advantage of the fact that you can export and remove environment variables during the activation and deactivation of micromamba virtual environment. To do so, you need to create two files with the following content:

1. `~/micromamba/envs/{{ cookiecutter.dev_env_name }}/etc/conda/activate.d/env_var.sh`:
```sh
export LAKEFS_ENDPOINT="http://127.0.0.1:8000/"
export LAKEFS_ACCESS_KEY="MY_ACCESS_KEY"
export LAKEFS_SECRET_KEY="MY_SECRET_KEY"
```

2. `~/micromamba/envs/{{ cookiecutter.dev_env_name }}/etc/conda/deactivate.d/env_var.sh`
```sh
unset LAKEFS_ENDPOINT
unset LAKEFS_ACCESS_KEY
unset LAKEFS_SECRET_KEY
```

**Contact the person in charge for sensitive information.**

### Adding research projects as submodules
Research projects are added to this project as git submodules. 

**It is recommended to create a new commit when adding/updating submodules.**

Research projects are added to the `research` folder with an indication of the author of the research:
```
git submodule add https://gitlab.com/<project-group>/{{ cookiecutter.repo_name }}-<author-name> research/<author-name>
```

### Start work
1. An engineering task is created to add steps to a pipeline or create a new pipeline.
1. A new branch is created in `lakefs` from the `dev` branch with the name `"{Task id}-{Task name}"`.
1. Work on the task is carried out in a separate `git` branch with the name `"{Task id}-{Task name}"`. This branch is created from the `dev` branch in `git`.
1. If the task is to update the pipeline, then select the file with the name of the pipeline. If the task is to create a new pipeline, including an experimental one, then create a file for the new pipeline `"/{{ cookiecutter.repo_name }}/{Pipeline name}.smk"`.
1. At the beginning of each pipeline, there should be a step to get data from a remote repository and create a local copy:
    ```snakemake
    rule load_data:
        input:
            s3_feather=S3.remote("<repo>/<commit>/<s3 path>/<file>"),
        output:
            feather=temp(Path("<local path>/<file>")),
        run:
            from pyarrow import feather
            data = feather.read_table(input.s3_feather)
            feather.write_feather(data, output.feather)
    ```
1. Then there are pipeline steps that transform the uploaded data (it's better to mark output as temp):
    ```snakemake
    rule simple_transformation:
        input:
            feather=Path("<local path>/<file>"),
        output:
            head=temp(Path("<local path>/<new file>")),
        run:
            from pyarrow import feather

            data = feather.read_table(input.feather)
            head = data.slice(length=5)
            feather.write_feather(head, output.head)
    ``` 
1. At the end, there should be a rule for uploading data to remote storage:
    ```
    rule upload_data:
        input:
            head=Path("<local path>/<new file>")
        output:
            remote_path=S3.remote("<repo>/<lakefs task branch>/<s3 path>/<file>")
        run:
            from pyarrow import feather

            data = feather.read_table(input.head)
            feather.write_feather(data, output.remote_path)
    ```
1. If you plan to use some code in other pipelines, then you can move it as a function to modules in the `src` folder.
1. Execute the pipeline with the command:
    ```cmd
    snakemake -s {{ cookiecutter.repo_name }}/{Pipeline name}.smk -c {max number of parallel jobs} {rule name}
    ```
1. Check in `lakefs` for uncommitted data in your branch. This data can be obtained by accessing the data in your branch, so you can check the result of the work or ask others to check.
1. If everything is fine, then commit the changes in your branch in `lakefs`.
1. Create a `Merge Request` in the `gitlib/github` to merge your branch into the `dev` branch and wait for approval.
1. If **your code is on the `dev` branch**, then you can merge your data branch into the `dev` branch in `lakefs`.

## How-to

### Micromamba
Some useful commands for `micromamba`. More about `micromamba` commands you can read on it's [documentation page][micromamba_docs_cli].

#### Activate environment
```
micromamba activate {{ cookiecutter.dev_env_name }}
```

#### Install package
```
micromamba install <package> -c conda-forge
```

### Poetry
Some useful commands for `poetry`. More about `poetry` commands you can read on it's [documentation page][poetry_docs_cli].

#### Add/Remove package
**Make sure you have the latest version of `pyproject.toml` and `poetry.lock` files.**

To add package manually:
```
poetry add <package>
```

Remove manually installed package in case something went wrong:
```
poetry remove <package>
```

Useful options for both `add` and `remove`:
- `--dev (-D)` add package as development dependency or remove from the development dependencies.
- `--dry-run` output operations but do not execute anything.

#### Show packages info
Show list of installed packages:
```
poetry show
```

Show package info and dependencies:
```
poetry show <package>
```

Show list of dependencies as a tree:
```
poetry show --tree
```

Show list of direct installed packages and their dependencies:
```
poetry show --only main
```

Show list of direct installed packages and their dependencies as a tree:
```
poetry show --only main --tree
```

### Hydra
`Hydra` allows you to streamline the maintenance of different configurations and their use. To work in `quarto` notebooks, `hydra compose api` is used, more about it you can read on it's [documentation page][hydra_compose_api_docs].

All configurations are stored in the `conf` folder. Sub folders of `conf` folder are groups within which you can choose one of the configurations to use, for example:
```
├── conf
    ├── config.yaml
    └── data
        ├── lakefs.yaml
        └── postgresql.yaml
```
You have two data sources, and each source needs its own set of configurations. You can create a `data` folder inside `conf` and specify the configuration for different sources in separate files.

Then you can specify inside the `config.yaml` that by default you need to use the `lakefs` source configuration in the data group:
```
# conf/config.yaml
defaults:
  - data: lakefs
```

Now, when composing the config, if you do not specify which config should be used for the `data` group, the `lakefs` config will be used by default.

#### Compose conf folder into one dictionary
```python
from hydra import compose, initialize

with initialize(version_base=None, config_path=<conf folder path>)):
    hydra_config = compose(config_name=<config name>)
```

#### Change Hydra configuration on composing
Let's use the example above. Suppose you need to change the default configuration of the `data` group for a specific task. Then, you can use the `overrides` parameter, and specify a different value for the `data` group:
```python
from hydra import compose, initialize

initialize(version_base=None, config_path=<conf folder path>))
    hydra_config = compose(config_name=<config name>,
                           overrides=["data=postgresql"])
```

**In the same way, you can overwrite values from config files at startup, but this is not recommended.**

#### Pass environment variables into config
```
# src/conf/data/lakefs.yaml

endpoint: ${oc.env:LAKEFS_ENDPOINT}
access_key: ${oc.env:LAKEFS_ACCESS_KEY}
secret_key: ${oc.env:LAKEFS_SECRET_KEY}
```

### Quarto
`Quarto` is an open-source scientific and technical publishing system built on `pandoc`. You can weave together narrative text and code to produce elegantly formatted output as documents, web pages, blog posts, books and more.
#### Install VSCode extension
Launch VS Code Quick Open (Ctrl+P), paste the following command, and press enter:
```
ext install quarto.quarto
```

### Activate completion

#### Bash
`Git` is installed immediately with a file for autocompletion, but for `poetry`, you must first generate a file:
```bash
poetry completions bash >> ~/micromamba/envs/{{ cookiecutter.dev_env_name }}/share/bash-completion/completions/poetry
```

Then just install `bash-completion` and restart your terminal:
```bash
micromamba install bash-completion -c conda-forge
```

[poetry_docs_cli]: https://python-poetry.org/docs/cli/
[micromamba_docs_cli]: https://mamba.readthedocs.io/en/latest/user_guide/micromamba.html
[hydra_compose_api_docs]: https://hydra.cc/docs/advanced/compose_api/
