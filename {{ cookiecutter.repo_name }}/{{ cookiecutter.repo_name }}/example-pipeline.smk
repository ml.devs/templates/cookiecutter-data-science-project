from pathlib import Path
from snakemake.remote.S3 import RemoteProvider as S3RemoteProvider

from src.config import compose_config

hydra_config = compose_config()

S3 = S3RemoteProvider(
    access_key_id=hydra_config.data.access_key,
    secret_access_key=hydra_config.data.secret_key,
    host=hydra_config.data.endpoint,
)


rule load_data:
    input:
        s3_feather=S3.remote("<repo>/<commit>/<s3 path>/<file>"),
    output:
        feather=temp(Path("<local path>/<file>")),
    run:
        from pyarrow import feather

        data = feather.read_table(input.s3_feather)
        feather.write_feather(data, output.feather)


rule simple_transformation:
    input:
        feather=Path("<local path>/<file>"),
    output:
        head=temp(Path("<local path>/<new file>")),
    run:
        from pyarrow import feather

        data = feather.read_table(input.feather)
        head = data.slice(length=5)
        feather.write_feather(head, output.head)


rule upload_data:
    input:
        head=Path("<local path>/<new file>"),
    output:
        remote_path=S3.remote("<repo>/<lakefs task branch>/<s3 path>/<file>"),
    run:
        from pyarrow import feather

        data = feather.read_table(input.head)
        feather.write_feather(data, output.remote_path)
