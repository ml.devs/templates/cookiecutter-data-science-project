from pathlib import Path

from hydra import compose, initialize
from hydra.compose import DictConfig


def compose_config(
    config_path: Path = Path("conf"), config_name: str = "config"
) -> DictConfig:
    """Get Hydra config dictionary.

    Initializes environment variables, composes configuration files and returns hydra
    config dictionary.

    Args:
        config_path (Path, optional): Path to conf folder. Defaults to Path("conf").
        config_name (str, optional): Config name from conf folder.
        Defaults to "config".

    Returns:
        DictConfig: Hydra config dictionary.
    """
    with initialize(version_base=None, config_path=str(config_path)):
        hydra_config = compose(config_name=config_name)
        return hydra_config
